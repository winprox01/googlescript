//Строит комбо-диаграмму
//Значения A2:B50

function onOpen() {

  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet()
  var menu = [{name: "Построить", functionName: "draw_table"}]
  spreadsheet.addMenu("Диаграмма", menu)
}

function draw_table() {
  
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet()
  var sheet = SpreadsheetApp.getActiveSheet()
  
  var chart = sheet.newChart()
     .setChartType(Charts.ChartType.COMBO)
     .addRange(sheet.getRange("A2:B50"))
     .setPosition(5, 5, 0, 0)
     .build()

  sheet.insertChart(chart)
}
