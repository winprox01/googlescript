//Создаёт папки для всех студентов
//Поля первой строки в таблице: ФИО, Почта
//Остальные соответственно заполнить данными

function onOpen() {

  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet()
  var menu = [{name: "Создать", functionName: "createFolders"}]
  spreadsheet.addMenu("Папки студентов", menu)
}

function createFolders() {
  
  var data = SpreadsheetApp.getActiveSheet().getRange(2, 1, 1000, 2).getValues()
  var folder = DriveApp.getRootFolder().createFolder("Студенты")
  
  for (i in data) {
    var id = data[i][0]
    if (id) {
      var inner_folder = folder.createFolder(id)
      inner_folder.addEditor([data[i][1]])
    }
  }
}
