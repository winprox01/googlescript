//Устанавливает ширину первого столбца в соответствии содержанию
//Делает копию таблицы
//Удаляет столбцы 2-10
//Любая таблица

function onOpen() {

  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet()
  var menu = [{name: "Выполнить", functionName: "backup"}]
  spreadsheet.addMenu("Выровнять, сделать Backup и удалить", menu)
}

function backup() {
  
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet()
  var sheet = SpreadsheetApp.getActiveSheet()
  
  sheet.autoResizeColumn(1)
  
  var spreadsheet_copy = spreadsheet.copy("Копия таблицы " + spreadsheet.getName())
  
  sheet.deleteColumns(2, 10)
  
  DriveApp.getRootFolder().createFile(spreadsheet_copy)
}
