//Удаляет из списка доступа к просмотру E-Mail адреса из 1 столбца
//Переименовывает активный лист в соответствии со второй строкой второго столбца
//Переименовывает файл в соответствии со второй строкой третьего столбца

function onOpen() {

  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet()
  var menu = [{name: "Выполнить", functionName: "change"}]
  spreadsheet.addMenu("Закрыть просмотр и изменить названия", menu)
}

function change() {
  
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet()
  var data = SpreadsheetApp.getActiveSheet().getRange(2, 1, 1000, 3).getValues()
  
  for (i in data) {
    var id = data[i][0]
    if (id) {
      spreadsheet.removeViewer([data[i][0]])
    }
  }
  
  spreadsheet.renameActiveSheet([data[0][1]].toString())
  spreadsheet.rename([data[0][2].toString()])
}
