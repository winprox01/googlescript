//Проверяет права доступа ко всем файлам в папке
//Папку указывать по ID в переменной "folder"

function onOpen() {
  
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet()
  var menu = [{name: "Анализировать доступ", functionName: "analyzingFolders"}]
  spreadsheet.addMenu("Файлы в папке", menu)
}
 
function analyzingFolders() {
  
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet()
  var folder = DriveApp.getFolderById("0Bw8yNqXOUmaEQUpjNkxXdmxVN3M")
  var contents = folder.getFiles()
  
  spreadsheet.appendRow(["Название", "Дата создания", "Размер", "Доступен к просмотру", "Доступен к редактированию"])
  
  while (contents.hasNext()) {
    
    var file = contents.next()
    
    var viewers = file.getViewers()
    var viewers_string = ""
    var editors = file.getEditors()
    var editors_string = ""
    
    for (var i = 0; i < viewers.length; i++) {
        viewers_string += " [Имя: " + viewers[i].getName() + ", Почта: " + viewers[i].getEmail() + "]"
    }
    for (var i = 0; i < editors.length; i++) {
        editors_string += " [Имя: " + editors[i].getName() + ", Почта: " + editors[i].getEmail() + "]"
    }
    
    spreadsheet.appendRow([file.getName(), file.getDateCreated(), file.getSize() + " byte", viewers_string, editors_string])
  }
}
